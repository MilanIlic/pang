package com.example.milani.pang.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by milani on 5.7.18..
 */

public class Spiral {

    float x, startY, topY;
    int ropeWidth = 15, arrowWidth = 15;
    float speed = 14;
    static Bitmap harpoonArrow, harpoonRope;

    public Spiral(float x, float y){
        this.x = x;
        this.startY = y;
        this.topY = y;
    }

    public void draw(Canvas canvas){

        canvas.drawBitmap(harpoonRope, null, new RectF(x - ropeWidth, topY + 60, x + ropeWidth, startY), new Paint());
        canvas.drawBitmap(harpoonArrow, null, new RectF(x - arrowWidth, topY, x + arrowWidth, topY + 60), new Paint());
        //canvas.drawRect(x, topY, x + 5, startY, new Paint(Color.BLUE));
    }

    public void updateSize(){
       topY -= speed;
    }
}
