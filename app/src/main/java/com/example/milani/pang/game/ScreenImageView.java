package com.example.milani.pang.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;

import com.example.milani.pang.R;

import java.util.ArrayList;

/**
 * Created by milani on 5.7.18..
 */

public class ScreenImageView extends SurfaceView implements SurfaceHolder.Callback{

    ImageData imageData;
    private float width, height;
    Bitmap backgroundBitmap;

    public ScreenImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        getHolder().addCallback(this);

        init();
    }

    void init() {

        imageData = new ImageData(getContext(), getHolder(), height, width);
        backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gamebackground);
    }


    public ImageData getImageData() { return imageData; }

    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH){
        imageData.onSizeChanged(w, h);
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);

        //canvas.drawBitmap(backgroundBitmap, getMatrix(), new Paint());
    }


    boolean drawOk;

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d("BALL", "SURFACE CREATED");

        Rect surfaceFrame = surfaceHolder.getSurfaceFrame();
        width = surfaceFrame.width();
        height = surfaceFrame.height();

        drawOk = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;
        imageData.updateThread.setRunning(false);

        drawOk = false;
        while (retry) {
            try {
                imageData.updateThread.interrupt();
                retry = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
