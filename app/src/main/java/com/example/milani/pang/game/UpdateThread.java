package com.example.milani.pang.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Created by milani on 5.7.18..
 */

public class UpdateThread extends Thread {
    private long time;
    private final int fps = 20;
    private boolean toRun = false;
    private ArrayList<Ball> balls;
    private ArrayList<Spiral> spirals;
    private Player player;
    private SurfaceHolder surfaceHolder;

    public UpdateThread(SurfaceHolder holder, ArrayList<Ball> balls, ArrayList<Spiral> spirals, Player player) {
        this.balls = balls;
        this.spirals = spirals;
        this.player = player;
        surfaceHolder = holder;
    }

    public void setRunning(boolean run) {
        toRun = run;
    }

    @Override
    public void run() {
        Log.d("THREAD", "RUNNING...");

        Canvas c;
        while (toRun) {

            long cTime = System.currentTimeMillis();

            if ((cTime - time) <= (1000 / fps)) {

                c = null;
                try {
                    if(toRun && !skipMode) {
                        c = surfaceHolder.lockCanvas();

                        if (c != null) {
                            c.drawColor(0, PorterDuff.Mode.CLEAR);

                            int spiralCount = spirals.size();
                            for (int i = 0; i < spiralCount; i++) {
                                Spiral spiral = spirals.get(i);
                                spiral.updateSize();
                                spiral.draw(c);
                            }

                            player.draw(c);

                            // check a conflict
                            checkConflict();

                            // remove spirals
                            removeSpirals();

                            int ballCount = balls.size();
                            if(ballCount == 0){
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Dialog.setNextLevelButton();
                                        Dialog.show();
                                    }
                                });
                                skipMode = true;
                            }

                            for (int i = 0; i < ballCount; i++) {
                                Ball ball = balls.get(i);
                                ball.updatePosition();
                                ball.draw(c);
                            }
                        }
                    }
                } catch (IllegalStateException e){

                }
                finally {
                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
            time = cTime;
        }

    }

    public void checkConflict(){

        ListIterator<Ball> iter = balls.listIterator();

        ArrayList<Ball> newBalls = new ArrayList<>();

        while(iter.hasNext()){
            Ball ball = iter.next();

            if(ball.isOverlaped(player)){
                // end of game
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Dialog.setTryAgainButton();
                        Dialog.show();
                    }
                });
                return;
            }

            int spiralCount = spirals.size();
            if(spiralCount > 0){
                Spiral spiral = spirals.get(0);

                if(ball.isOverlaped(spiral)){
                    // TODO: resize this ball and add another one
                    if(ball.radius > 40) {
                        // clone the ball
                        Ball newBall = new Ball(ball.x, ball.y, ball.radius / 2);

                        newBall.xVelocity = ball.xVelocity * (-1); // change direction of the new ball
                        newBall.yVelocity = Ball.yVelocityInit; // direction down
                        newBall.y = newBall.y - 40; // put the ball in a higher position

                        newBalls.add(newBall);

                        ball.y = ball.y - 40; // put the ball in a higher position

                        ball.yVelocity = Ball.yVelocityInit; // direction down
                        ball.radius = ball.radius / 2;

                    }else
                        iter.remove(); // too small ball

                    // destroy spiral
                    spirals.remove(0);
                    break;
                }
            }
        }

        // add all new balls
        balls.addAll(newBalls);
    }

    public void removeSpirals(){
        ListIterator<Spiral> iter = spirals.listIterator();

        while(iter.hasNext()){
            if(iter.next().topY <= 0)
                iter.remove();
        }
    }

    boolean skipMode;
    public void setSkipMode(boolean skipMode) {
        this.skipMode = skipMode;
    }
}
