package com.example.milani.pang.game;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.milani.pang.GameActivity;
import com.example.milani.pang.R;

import java.util.ArrayList;

/**
 * Created by milani on 5.7.18..
 */

public class ImageData {

    float width, height;
    Context context;
    Resources resources;
    Player player;


    ArrayList<Ball> balls = new ArrayList();
    ArrayList<Spiral> spirals = new ArrayList<>();

    public UpdateThread updateThread;

    public ImageData(Context context, SurfaceHolder surfaceHolder, float height, float width){
        this.context = context;
        this.height = height;
        this.width = width;

        Player.leftBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.leftcharacter);
        Player.rightBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.rightcharacter);
        Player.activeBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.chactive);

        Ball.ballBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ball);

        Spiral.harpoonArrow = BitmapFactory.decodeResource(context.getResources(), R.drawable.harpoonarrow);
        Spiral.harpoonRope = BitmapFactory.decodeResource(context.getResources(), R.drawable.harpoonrope);

        player = new Player(height, width);

        updateThread = new UpdateThread(surfaceHolder, balls, spirals, player);
        updateThread.setRunning(true);
        updateThread.start();
        resources = context.getResources();
        // get image position

    }

    public void down(float x, float y) {
        Ball newBall = new Ball(x, y, 50);
        balls.add(newBall);
    }

    public void addBall(Ball newBall) {
        balls.add(newBall);
    }

    public void onSizeChanged(int w, int h) {
        width = w;
        height = h;

        Log.d("SIZE", "" + height + ", " + width);
        updatePositions();
    }

    private void updatePositions() {
        player.onSizeChanged(width, height);
        Ball.onSizeChanged(width, height);
    }


    public void addSpiral(float x) {
        // boosters -> allow to fire more spirals
        if(spirals.size() == 0)
            spirals.add(new Spiral(x, height));

    }

    public void deleteAllBalls() {
        balls.clear();
    }

    public Player getPlayer(){
        return player;
    }
}
