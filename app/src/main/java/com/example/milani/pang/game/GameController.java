package com.example.milani.pang.game;

import android.content.Context;
import android.graphics.PointF;
import android.view.View;

import com.example.milani.pang.GameActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by milani on 5.7.18..
 */

public class GameController {



    public interface ViewInterface {
        void updateImage();
    }

    private ViewInterface view;
    private ImageData imageData;

    Player player;

    static HashMap<Integer, ArrayList<Ball>> levelsMap;

    static{
        levelsMap = new HashMap<>();

        ArrayList<Ball> firstLevel = new ArrayList<>();
        firstLevel.add(new Ball(1000, 135, 110));

        levelsMap.put(1, firstLevel);

        ArrayList<Ball> secondLevel = new ArrayList<>();
        secondLevel.add(new Ball(430, 135, 130, -3, 2));
        secondLevel.add(new Ball(1500, 135, 130));

        levelsMap.put(2, secondLevel);

        ArrayList<Ball> thirdLevel = new ArrayList<>();
        thirdLevel.add(new Ball(300, 200, 200));
        levelsMap.put(3, thirdLevel);

        ArrayList<Ball> fourthLevel = new ArrayList<>();
        fourthLevel.add(new Ball(100, 100, 150));
        fourthLevel.add(new Ball(400, 100, 150));
        fourthLevel.add(new Ball(700, 100, 150));
        fourthLevel.add(new Ball(1000, 100, 150));
        fourthLevel.add(new Ball(1300, 100, 150));
        levelsMap.put(4, fourthLevel);



        ArrayList<Ball> fifthLevel = new ArrayList<>();
        fifthLevel.add(new Ball(100, 100, 50));
        fifthLevel.add(new Ball(200, 200, 50));
        fifthLevel.add(new Ball(300, 100, 50));
        fifthLevel.add(new Ball(400, 200, 50));
        fifthLevel.add(new Ball(500, 100, 50));
        fifthLevel.add(new Ball(600, 200, 50));
        fifthLevel.add(new Ball(700, 100, 50));
        fifthLevel.add(new Ball(800, 200, 50));
        fifthLevel.add(new Ball(900, 100, 50));
        fifthLevel.add(new Ball(1000, 200, 50));
        levelsMap.put(5, fifthLevel);
    }

    public GameController(ViewInterface gameActivity, ImageData imageData, int level) {
        this.view = gameActivity;
        this.imageData = imageData;
        this.player = imageData.player;

        for(Ball b: levelsMap.get(level)) {
            imageData.addBall(new Ball(b.x, b.y, b.radius));
        }

        view.updateImage();
        //updateScreen();
    }


    void updateScreen(){
        View decorView = ((GameActivity)view).getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }



    public void cancel() {
    }

    public void down(float x, float y) {
        imageData.down(x, y);
        view.updateImage();
    }

    public void fire(){
        player.fire();
        imageData.addSpiral(player.centerX() + 15); // because of harpoon gun position
        view.updateImage();
    }

    public void turnLeft(){
        player.moveLeft();
        view.updateImage();
    }

    public void turnRight(){
        player.moveRight();
        view.updateImage();
    }

}