package com.example.milani.pang.intro;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.AttributeSet;

public class TypeWriter extends android.support.v7.widget.AppCompatTextView {

    private CharSequence mText;
    private int mIndex;
    private long mDelay = 500; //Default 500ms delay

    public AsyncTask<Void, Void, Void> asyncTask;



    public TypeWriter(Context context) {
        super(context);
    }

    public TypeWriter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private Handler mHandler = new Handler();

    private Runnable characterAdder;


    public void animateText(CharSequence text) {
        mText = text;
        mIndex = 0;

        asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                characterAdder = new Runnable() {
                    @Override
                    public synchronized void run() {
                        setText(mText.subSequence(0, mIndex++));
                        if(mIndex <= mText.length()) {
                            mHandler.postDelayed(characterAdder, mDelay);
                        }
                        if(mIndex == mText.length() && notifyWriter != null){
                            notifyWriter.startTyping();
                        }
                    }
                };

                setText("");

                mHandler.removeCallbacks(characterAdder);
                mHandler.postDelayed(characterAdder, mDelay);
                return null;
            }
        };
    }

    public void startTyping() {
        asyncTask.execute();

        setText("");
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder, mDelay);
    }

    public void setCharacterDelay(long millis) {
        mDelay = millis;
    }


    TypeWriter notifyWriter;

    public void onFinishNotify(TypeWriter writer) {
        notifyWriter = writer;
    }

}