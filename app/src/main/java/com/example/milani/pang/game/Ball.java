package com.example.milani.pang.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by milani on 5.7.18..
 */

public class Ball{

    Paint paint;
    float radius;
    static Bitmap ballBitmap;

    float x, y;

    float xVelocity, yVelocity;
    static float screenHeight, screenWidth;
    static float xVelocityInit = 3, yVelocityInit = 2;

    public Ball(float x, float y, float radius){
        Log.d("BALL", "BALL CREATION [ " + x + ", " + y + " ]");
        this.x = x;
        this.y = y;
        this.radius = radius;
        paint = new Paint(Color.BLACK);

        xVelocity = xVelocityInit;
        yVelocity = yVelocityInit;
    }

    public Ball(float x, float y, float radius, float xVel, float yVel){
        this(x, y, radius);
        xVelocity = xVel;
        yVelocity = yVel;
    }

    // draw the bitmap with ball
    public void draw(Canvas canvas){
        //Log.d("BALL", "Drawing");
        canvas.drawBitmap(ballBitmap, null, new RectF(this.x - radius, y - radius, x + radius, y + radius), new Paint());
    }

    public static void onSizeChanged(float w, float h){
        screenHeight = h;
        screenWidth = w;
    }

    public void updatePosition(){
        x += xVelocity;
        y += yVelocity;

        if((y - radius) < 0 || (y + radius) > screenHeight){
            if((y - radius) < 0){
                // the ball has hit the top of the canvas
                y = radius;
            }else{
                // the ball has hit the bottom of the canvas
                y = screenHeight - radius;
            }

            // reverse the ball direction
            yVelocity *= -1;
        }

        if((x - radius) < 0 || (x + radius) > screenWidth){
            if(x - radius < 0){
                x = radius;
            }else
                x = screenWidth - radius;

            // reverse the ball direction
            xVelocity *= -1;
        }
    }

    public boolean isOverlaped(Spiral spiral){
        if((y + radius) > spiral.topY)
            if(((x - radius) < spiral.x && (x + radius) > spiral.x))
                return true;

        return false;
    }

    public boolean isOverlaped(Player player) {
        if((y + radius) > (player.top + 25)) // +25 -> makes it more real
            if(((x - radius) < player.left && (x + radius) > (player.left + 25)) || // +15 -> makes it more real
                    ((x - radius) < (player.right - 25) && (x + radius) > player.right) || // -15 -> makes it more real
                    ((x - radius) > player.left && (x + radius) < player.right))
                return true;

        return false;
    }
}
