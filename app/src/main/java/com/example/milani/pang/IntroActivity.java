package com.example.milani.pang;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.milani.pang.intro.TypeWriter;

public class IntroActivity extends AppCompatActivity {


    String introText1 = "Copacabana, a well known beach-soccer capital, has been ruled by boys and balls for many years.\n"+
            "A little girl called Ana was tired of the poor football performances by the boys and one day decided to show them who’s BOSS!\n";
    String introText2 = "But the boys didn’t let her play…\n"+
            "She just smiled and went home to get her harpoon which she bought at a local market for a bargain…";

    MediaPlayer mp;

    int delay = 70;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        TypeWriter writer1 = (TypeWriter)(findViewById(R.id.introTypeWriter1));
        TypeWriter writer2 = (TypeWriter)(findViewById(R.id.introTypeWriter2));

        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/04b30.ttf");
        writer1.setTypeface(face);
        writer2.setTypeface(face);

        mp = MediaPlayer.create(IntroActivity.this, R.raw.typing);
        mp.setLooping(true);


        writer1.onFinishNotify(writer2);
        writer1.setCharacterDelay(delay);
        writer1.animateText(introText1);
        writer1.startTyping();
        mp.start();

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                try {
                    mp.stop();
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }, delay * (introText1.length() + introText2.length()));



        writer2.setCharacterDelay(delay);
        writer2.animateText(introText2);

    }

    public void startGame(View view) {
        Intent gameIntent = new Intent(this, GameActivity.class);
        // best score and other args
        mp.stop();

        startActivity(gameIntent);
        finish();
    }
}
