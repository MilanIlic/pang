package com.example.milani.pang.game;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.milani.pang.R;

/**
 * Created by milani on 27.6.18..
 */

public class Dialog {

    public static AlertDialog dialog;
    public static View view;
    public static TextView scoreTextView;
    public static ImageView imageView;
    public static Button resultButton;
    public static Button quitButton;
    public static boolean isVisible;
    public static View.OnClickListener tryAgainListener, nextLevelListener;

    public static void show(){ if(isVisible)dialog.show(); }
    public static void setTryAgainButton(){
        resultButton.setBackgroundResource(R.drawable.tryagainbackground);
        view.setBackgroundResource(R.drawable.gameoverpreview);
        resultButton.setOnClickListener(tryAgainListener);
    }

    public static void setNextLevelButton(){
        resultButton.setBackgroundResource(R.drawable.nextlevelbackground);
        view.setBackgroundResource(R.drawable.endgamebackground);
        resultButton.setOnClickListener(nextLevelListener);
    }

    public static void setDialog(final AlertDialog alertDialog){
        dialog = alertDialog;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
