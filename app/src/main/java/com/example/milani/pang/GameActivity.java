package com.example.milani.pang;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.milani.pang.game.Ball;
import com.example.milani.pang.game.Dialog;
import com.example.milani.pang.game.GameController;
import com.example.milani.pang.game.ImageData;
import com.example.milani.pang.game.RepeatListener;
import com.example.milani.pang.game.ScreenImageView;

public class GameActivity extends AppCompatActivity implements View.OnTouchListener, GameController.ViewInterface{

    GameController gameController;
    ScreenImageView imageView;
    ImageData imageData;
    MediaPlayer mp;
    SharedPreferences sharedPref;

    int level = 1;


    Button turnLeft, turnRight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mp = MediaPlayer.create(GameActivity.this, R.raw.copa);
        mp.setVolume(0.6f, 0.6f);
        mp.setLooping(true);

       /* View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        init();
    }

    private void init(){

        // fetch the level
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        level = 1; //sharedPref.getInt(getString(R.string.level), 1);

        imageView = findViewById(R.id.imageView);
        imageData = imageView.getImageData();
        //imageView.setOnTouchListener(this);

        gameController = new GameController(this, imageData, level);

        turnLeft = (Button)(findViewById(R.id.leftBUtton));
        turnRight = (Button)(findViewById(R.id.rightBUtton));

        turnLeft.setOnTouchListener(new RepeatListener(300, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goLeft();
            }
        }));

        turnRight.setOnTouchListener(new RepeatListener(300, 100, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goRight();
            }
        }));

        // init dialog
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(GameActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.game_dialog, null);
        mBuilder.setView(mView);

        Dialog.view = mView;
        Dialog.resultButton = (Button) mView.findViewById(R.id.tryAgainButton);
        Dialog.quitButton = (Button) mView.findViewById(R.id.quitButton);
        Dialog.setDialog(mBuilder.create());

        Dialog.dialog.setCanceledOnTouchOutside(false);
        Dialog.isVisible = true;

        Dialog.quitButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                GameActivity.this.finish();
            }
        });

        Dialog.tryAgainListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageData.deleteAllBalls();
                imageData.getPlayer().setStartPosition();
                gameController = new GameController(GameActivity.this, imageData, level);
                Dialog.dialog.cancel();
            }
        };

        Dialog.nextLevelListener = new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d("NEXT LEVEL", "NEXT LEVEL!");
                imageData.deleteAllBalls();
                imageData.getPlayer().setStartPosition();
                level++;
                imageData.updateThread.setSkipMode(false);
                gameController = new GameController(GameActivity.this, imageData, level);
                Dialog.dialog.cancel();
            }
        };

        mp.start();
    }

    private void updateLevel() {
        level++;

    }

    private int getLevel() {
        return level;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event){
        float x = event.getX(), y = event.getY();
        Log.d("TOUCH", "Selected: [ " + x + ", " + y + " ]");

        switch (event.getActionMasked()){
            case MotionEvent.ACTION_DOWN:
                //gameController.down(x, y);
                break;
            case MotionEvent.ACTION_CANCEL:
                gameController.cancel();
                break;
        }

        return true;
    }

    @Override
    public void finish(){
        super.finish();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.level), level);
        editor.commit();

        mp.stop();
        mp.reset();
    }

    @Override
    public void updateImage() {
        imageView.invalidate();
    }

    public void goLeft() {
        gameController.turnLeft();
    }

    public void goRight() {
        gameController.turnRight();
    }

    public void fire(View view) {
        gameController.fire();
    }
}
