package com.example.milani.pang.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by milani on 5.7.18..
 */

public class Player extends RectF {

    float size = 200, activeSize = 90;
    int step = 40;
    static Bitmap rightBitmap, leftBitmap, activeBitmap;
    Bitmap bitmap;
    float screenWidth, screenHeight;

    public Player(float screenHeight, float screenWidth){
        bitmap = leftBitmap;

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public void setStartPosition(){
        left = screenWidth/2;
        right = left + size;
        bottom = screenHeight;
        top = bottom - size;
    }

    public void updatePosition(float x){
        if((x - size/2) < 0){
            left = 0;
            right = size;
        }
        else if((x + size/2) > screenWidth){
            right = screenWidth;
            left = right - size;
        } else {
            left = x - size / 2;
            right = x + size / 2;
        }
    }

    public void setInitWidth(){
        left = centerX() - size/2;
        right = centerX() + size/2;
    }

    public void setActiveWidth(){
        left = centerX() - activeSize/2;
        right = centerX() + activeSize/2;
    }

    public PointF getPoint(){
        return new PointF(centerX(), centerY());
    }

    public void draw(Canvas canvas){
        if(bitmap != null)
            canvas.drawBitmap(bitmap, null, this, new Paint());
    }

    public void onSizeChanged(float width, float height) {
        this.screenWidth = width;
        this.screenHeight = height;

        setStartPosition();
    }


    public void simulateMoving(final float start, final float dest){
        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                float pos = start;

                if(start > dest)
                    while(pos > dest){ // move to left
                        pos -= 2;
                        updatePosition(pos);
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                else{
                    while(pos < dest){ // move to right
                        pos += 2;
                        updatePosition(pos);
                        try {
                            Thread.sleep(15);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return null;
            }
        }).execute();
    }

    public void moveLeft() {
        if(bitmap != leftBitmap) {
            setInitWidth();
            bitmap = leftBitmap;
        }
        updatePosition(centerX() - step);
        //simulateMoving(centerX(), centerX() - step);
    }

    public void moveRight() {
        if(bitmap != rightBitmap) {
            setInitWidth();
            bitmap = rightBitmap;
        }
        updatePosition(centerX() + step);
        //simulateMoving(centerX(), centerX() + step);
    }

    public void fire() {
        if(bitmap != activeBitmap) {
            bitmap = activeBitmap;
            setActiveWidth();
        }
    }

}
